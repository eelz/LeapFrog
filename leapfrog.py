import sys
import time
import os
import pathlib


if len(sys.argv) != 3:
    print("Usage: " +__file__+" {submit|download} <exercism_name>")

    sys.exit(3)

command = sys.argv[1]
exercism_name = sys.argv[2]

if command == 'download':
    T1 = time.time()
    print(os.environ['HOME'])
    print(os.path.abspath('.'))
    print(T1)
    print('download')

elif command == 'submit':
    T2 = time.time()
    print(T2)
    print(T2-T1)
    print('submit') 

else:
    print("Error: Command not found. Please use 'submit' or 'download'")

